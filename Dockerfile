FROM openjdk:8-jre-alpine@sha256:f362b165b870ef129cbe730f29065ff37399c0aa8bcab3e44b51c302938c9193

ARG SCALA_VERSION=2.11.7
ARG SBT_VERSION=1.3.9
ENV SML_ENV=staging
ENV SCALA_HOME=/usr/share/scala

#Install scala from $ARG
RUN \
  apk add --no-cache --virtual=.build-dependencies wget ca-certificates && \
  apk add --no-cache bash && \
  cd "/tmp" && \
  wget "https://downloads.typesafe.com/scala/${SCALA_VERSION}/scala-${SCALA_VERSION}.tgz" && \
  tar xzf "scala-${SCALA_VERSION}.tgz" && \
  mkdir "${SCALA_HOME}" && \
  rm "/tmp/scala-${SCALA_VERSION}/bin/"*.bat && \
  mv "/tmp/scala-${SCALA_VERSION}/bin" "/tmp/scala-${SCALA_VERSION}/lib" "${SCALA_HOME}" && \
  ln -s "${SCALA_HOME}/bin/"* "/usr/bin/" && \
  apk del .build-dependencies && \
  rm -rf "/tmp/"*

#Install SBT from $ARG
RUN \
  apk add --no-cache --virtual=.build-dependencies bash curl bc ca-certificates && \
  cd "/tmp" && \
  update-ca-certificates && \
  scala -version && \
  scalac -version && \
  curl -fsL https://github.com/sbt/sbt/releases/download/v$SBT_VERSION/sbt-$SBT_VERSION.tgz | tar xfz - -C /usr/local && \
  $(mv /usr/local/sbt-launcher-packaging /usr/local/sbt || true) && \
  ln -s /usr/local/sbt/bin/* /usr/local/bin/ && \
  apk del .build-dependencies && \
  rm -rf "/tmp/"*

#Prevent issue sbt/sbt#1458
RUN \
  sbt -Dsbt.rootdir=true -batch sbtVersion && \
  rm -rf project target
  
COPY [".","/"]
RUN \
  sbt assembly
#Port exposing
EXPOSE 8081/tcp
EXPOSE 8081/udp

#Prod entrypoint:
ENTRYPOINT ["java", "-jar", "./target/scala-2.11/joinus-devops-service-assembly-0.0.1-SNAPSHOT.jar"]
#Debug entry point
#ENTRYPOINT [ "tail", "-f", "/dev/null" ]