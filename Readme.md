
# Sofwaremill Scala API APP
API Reference and short describiton about "How to" run microservice on local device via docker compose / Makefile 




## API Reference

#### Get all items

```http
  GET /{Parameter}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `status` | `string` | Show JSON status about condition  |
| `environment` | `string` | Returns the name of the environment the application is running on, taken from the container environment variable **SML_ENV** |


```http
  POST /{Parameter}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `dowork?magicnumber=[number]` | `string` | Returns the result of the application's logic  |


## Docker Compose

```http
docker compose up
```
Will run application with simple healthcheck on http://localhost:8081 

## Makefile
To manage your images and deployment, you can use the **MAKE** command with one of the parameters
```http
# DOCKER TASKS
## Build the container actions:
build: ## Build the container

build-nc: Build the container without caching
run: Run container on port and with volumens configured in `config.env
up: Run container on port configured in `config.env` (Alias to run)
stop: Stop and remove a running container
release: Make a release by building and publishing the `{version}` ans `latest` tagged containers to ECR

##Docker publish container tasks:
publish-latest: tag-latest Publish the `latest` taged container to ECR
publish-version: tag-version Publish the `{version}` taged container to ECR

##Docker tagging container tasks:
tag: tag-latest tag-version Generate container tags for the `{version}` ans `latest` tags
tag-latest: Generate container `{version}` tag
tag-version: Generate container `latest` tag

version: Output the current version
```

## Example values from config files:

```html
##VERSION:

# Example version script.
# Please choose one version or create your own

# Node.js: grep the version from a package.json file with jq
#jq -rM '.version' package.json

# Elixir: grep the version from a mix file
#cat mix.exs | grep version | grep '\([0-9]\+\.\?\)\{3\}' -o


##CONFIG ENV FILE:

# Port to run the container 
PORT=8081
APP_NAME=softwaremill-scala-api
VOLUME_NAME=softwaremill-logs
CONTAINER_PATH_PERSISTENCE=/app/logs

# Until here you can define all the individual configurations for your app

```

## Authors

- [@Rafal Klimczak](https://www.linkedin.com/in/rafalklimczak1989/)
- [@Makefile template:](https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db)

## Q&A

**1. Are there any assumptions in your solution that we should be aware of during the code review?**

```html
I wrote the solution during the summer break, so I hope that there were no discrepancies in the names of the variables, etc. I also apologize for the waiting time for my solution.I also tried to be compliant with the versions of the packages you proposed and please do not pay attention to the security considerations of JRE images. I know it can be done in a more secure way :) I made one change to the location of the logs because it suited me better for docker-compose and CI / CD
```

**1. What technologies have you chosen to implement the solution? Why?**
```html
1) Docker:
I used official images from DockerHUB and installed all the necessary dependencies on it as described in DOCKERFILE
2) Docker compose:
As recommended in the task, I described the environment construction and basic healthcheck as docker-compose.yml
3) Makefile:
I used the template to create a Makefile so that tasks such as construction, versioning, deployment in the AWS environment can be standardized and automated
4) Gitlab CI/CD:
I also wrote a simple 3-stage CI / CD pipeline for construction, basic local tests and API microservice deployment on AWS ElasticBeanStalk

PS:
I also read (it is also on your blog) that generating docker images can be implemented as part of SBT. However, I did not want to interfere with the application code

PS2:
Preconfigured GitLab environment variables are implemented on a global level so they are not in the code
```
**3. How would you plan to deploy this application into the real cloud infrastructure? What steps
should be included in this plan? What tools/services would you suggest and why?**
```html
For deployment, I used the simplest method, AWS ElasticBeanStalk. As for productions, depending on the complexity of the application, AWS ECS / AWS Fargate or the k8s / OpenShift cluster could be used. To operate databases one of the services such as RDS or Aurora, to operate the queue, e.g. Redis, to operate the Route53 domain. There are many possibilities, but I would use k8s / OpenShift as a precaution if this application had the potential of a significant increase in microservices or a possible migration to another cloud provider.
```